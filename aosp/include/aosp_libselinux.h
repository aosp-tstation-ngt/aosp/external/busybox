#ifndef BB_AOSP_LIBSELINUX_H
#define BB_AOSP_LIBSELINUX_H

#if ENABLE_SELINUX
/* MATCHPATHCON_xxx is defined 
 * in external/selinux/libselinux/include/selinux/selinux.h
 * but not
 * in external/libselinux/include/selinux/selinux.h
 */
#ifndef MATCHPATHCON_BASEONLY
#define MATCHPATHCON_BASEONLY 1	/* Only process the base file_contexts file. */
#define MATCHPATHCON_NOTRANS  2	/* Do not perform any context translation. */
#define MATCHPATHCON_VALIDATE 4	/* Validate/canonicalize contexts at init time. */
#endif


// defined in matchpathcon.c
extern int  matchpathcon(const char *path, mode_t mode, char ** con);
extern void matchpathcon_checkmatches(char *str __attribute__((unused)));
extern int  matchpathcon_filespec_add(ino_t ino, int specind, const char *file);
extern void matchpathcon_filespec_destroy(void);
extern void matchpathcon_filespec_eval(void);
extern void matchpathcon_fini(void);
extern int  matchpathcon_index(const char *name, mode_t mode, char ** con);
extern int  matchpathcon_init(const char *path);
extern int  matchpathcon_init_prefix(const char *path, const char *subset);
extern int  selinux_file_context_verify(const char *path, mode_t mode);
extern void set_matchpathcon_canoncon(int (*f) (const char *p, unsigned l, char **c));
extern void set_matchpathcon_flags(unsigned int flags);
extern void set_matchpathcon_printf(void (*f) (const char *fmt, ...));
//int  selinux_file_context_cmp(const char * a, const char * b);
//int  selinux_lsetfilecon_default(const char *path);
//int  realpath_not_final(const char *name, char *resolved_path);
//void set_matchpathcon_invalidcon(int (*f) (const char *p, unsigned l, char *c));


// delegate to libselinux's implementation
static inline int security_canonicalize_context_raw(const char * con, char ** canoncon)
{
  return security_canonicalize_context(con, canoncon);
}
static inline int is_context_customizable(const char * scontext __attribute__((unused)))
{ return 0; }
static inline int lgetfilecon_raw(const char *path, char ** context)
{
  return lgetfilecon(path, context);
}
static inline int lsetfilecon_raw(const char *path, const char * context)
{
  return lsetfilecon(path, context);
}
static inline int selinux_getenforcemode(int *enforce)
{
  return enforce ? (*enforce = security_getenforce(), 0) : -1;
}
static inline const char *selinux_policy_root(void)
{
  return "/sepolicy";
}
/* The stub of selabel_lookup_raw() delegating to selabel_lookup() is required,
 * because libselinux has a declaration of selabel_lookup_raw, but not the definition,
 *
 * But static inline int selabel_lookup_raw(..) conflicts with it's declaration.
 * So to prevent the conflict, define it as a macro.
 */
#define selabel_lookup_raw(rec,con,key,type)  selabel_lookup(rec,con,key,type)


#endif  // ENABLE_SELINUX

#endif  // BB_AOSP_LIBSELINUX_H
