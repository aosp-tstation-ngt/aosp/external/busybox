#ifndef BB_AOSP_ANDROID_H
#define BB_AOSP_ANDROID_H


#include  "aosp_if_ether.h"  // for struct ether_arp

// syscalls
#if defined(ANDROID) || defined(__ANDROID__)
pid_t getsid(pid_t pid);
int stime(const time_t *t);
int sethostname(const char *name, size_t len);
struct timex;
int adjtimex(struct timex *buf);
int pivot_root(const char *new_root, const char *put_old);
int shmget(key_t key, size_t size, int shmflg);
int shmdt(const void *shmaddr);
void *shmat(int shmid, const void *shmaddr, int shmflg);
int msgget(key_t key, int msgflg);
int semget(key_t key, int nsems, int semflg);
struct msqid_ds; /* #include <linux/msg.h> */
int msgctl(int msqid, int cmd, struct msqid_ds *buf);
struct shmid_ds; /* #include <linux/shm.h> */
int shmctl(int shmid, int cmd, struct shmid_ds *buf);
struct sembuf; /* #include <linux/sem.h> */
int semop(int semid, struct sembuf *sops, unsigned nsops);
# if __ANDROID_API__ < 21
int tcdrain(int fd);
# endif
# endif



#ifdef HAVE_MNTENT_H
# ifndef MNTOPT_NOAUTO
#  define MNTOPT_NOAUTO	"noauto"	/* Do not auto mount.  */
# endif
char *hasmntopt (const struct mntent *mnt, const char *opt);
#endif



#if ENABLE_SELINUX
#include  "aosp_libselinux.h"
#endif


// used in networking/arping.c
#ifdef CONFIG_ARPING
#include <sys/socket.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#endif

// used in networking/[ether-wake.c, nameif.c]
#if defined(CONFIG_ETHER_WAKE) || defined(CONFIG_NAMEIF)
#include "../../networking/ether_port.h"
#endif


#endif // BB_AOSP_ANDROID_H
