This repository is built with ndk-box-kitchen's script.
  ndk-box-kitchen download site:
    https://github.com/topjohnwu/ndk-box-kitchen.git
    commit 983c7687ee093aee77522c8a22f9b7221fe19395


This build is a little different from the way of ndk-box-kitchen:

                    build scripts     libselinux
  ---------------------------------------------------------------------------
  ndk-box-kitchen   NDK               github.com/SELinuxProject/selinux.git
  this build        AOSP              AOSP's external/libselinux

  note:
      AOSP has also external/selinux/libselinux,
      and it's almost same as github.com/SELinuxProject/selinux.git:
        at commit 9d76b62fa744cfdd1cf48d83ade2401f2f889abd .
      But it is not used in AOSP.

The build steps are:
  1. download ndk-box-kitchen into the working directory './bb-work'
     mkdir -p ./bb-work/busybox
     cd ./bb-work
     git clone --depth=1 https://github.com/topjohnwu/ndk-box-kitchen.git ./

  2. download busybox source and extract into ./busybox
     https://www.busybox.net/
     busybox-1.31.1.tar.bz2

     The source corresponds to:
       git://busybox.net/busybox.git
       at commit bd754746394a382e04d116df02547f61b2026da9

  3. apply patch and generate required files.
     ( The busybox's configuration is ./busybox.config. )

     execute "./busybox.sh patch"     to apply patches
     execute "./busybox.sh generate"  to generate required Makefiles and headers

  4. copy ./busybox into AOSP's external/busybox
     and initialize git repository with initial commit.
     note:
       Some files are created at step 3, which are not used in AOSP build system.
       They are excluded by ".gitignore", so automatically removed when the repository is cloned.
       The files are:
         xxx/Kbuild
         xxx/Config.in
         xxx.orig
         xxx.cmd
         ./.config.old
         ./.kconfig.d
         ./.kernelrelease
         ./applets/applet_tables
         ./applets/usage
         ./scripts/basic/docproc
         ./scripts/basic/fixdep
         ./scripts/basic/split-include
         ./scripts/kconfig/conf
         ./scripts/kconfig/conf.o
         ./scripts/kconfig/kxgettext.o
         ./scripts/kconfig/lex.zconf.c
         ./scripts/kconfig/mconf.o
         ./scripts/kconfig/zconf.hash.c
         ./scripts/kconfig/zconf.tab.c
         ./scripts/kconfig/zconf.tab.o



  And some modification are made.

  5. delete ./include/.gitignore
     to include generated headers ( at step 3 ) into the git repository.

  6. add libcrypto_static to LOCAL_STATIC_LIBRARIES
     in Android.mk
     because libselinux needs this.
     ( without this, the build fails with an error message "undefined SHA1". )

  7. add missing definitions
     into ./aosp/xxx.
     Most of them are copied from:
       external/libselinux/src/
       external/selinux/libselinux/src/
       prebuilts/ndk/current/platforms/android-24/arch-arm/usr/include/
       prebuilts/gcc/linux-x86/host/x86_64-linux-glibc2.15-4.8/sysroot/usr/include/


