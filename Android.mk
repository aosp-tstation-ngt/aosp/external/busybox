LOCAL_PATH := $(call my-dir)
VERSION = 1
PATCHLEVEL = 31
SUBLEVEL = 1
EXTRAVERSION = -Magisk
BB_VER = $(VERSION).$(PATCHLEVEL).$(SUBLEVEL)$(EXTRAVERSION)




##########################
# busybox-aosp

include $(CLEAR_VARS)
LOCAL_MODULE := busybox-aosp
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_STATIC_LIBRARIES := libselinux libcrypto_static
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/utilities

include $(LOCAL_PATH)/Android_src.mk
LOCAL_SRC_FILES += aosp/selinux/matchpathcon.c


LOCAL_LDFLAGS := -static
LOCAL_CFLAGS := \
-w -include include/autoconf.h -D__USE_BSD \
-DBB_VER=\"$(BB_VER)\" -DBB_BT=AUTOCONF_TIMESTAMP

LOCAL_FORCE_STATIC_EXECUTABLE := true

# This works in ndk build, but is ignored in AOSP build.
LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
# some sources cause compile error
#   error: format not a string literal and no format arguments [-Werror=format-security]
# so override -Werror=format-security
LOCAL_CFLAGS += -Wno-error=format-security


LOCAL_C_INCLUDES += $(LOCAL_PATH)/aosp/include


# There seems to be incomatiblity between gcc and clang,
# Clang build causes Seg-fault at: 
#   INIT_G_misc macro in shell/ash.c
#		INIT_S macro in ibbb/lineedit.c
# The following code seems to cause problems:
#		struct ST;
#		struct ST*const pST;
#		void a_func()
#		{
#		  *(ST**)(&pSTC) = (ST*)sume_address;
#		}
# Removing 'const' qualifier eliminates the problem,
# but it seems better to back to gcc.
LOCAL_CLANG := false


# also generate busybox-applets.list
BB_APPLETS_LIST := busybox-applets.list
LOCAL_ADDITIONAL_DEPENDENCIES += $(LOCAL_MODULE_PATH)/$(BB_APPLETS_LIST)


include $(BUILD_EXECUTABLE)


########################
# generate busybox-applets.list
include $(CLEAR_VARS)


LOCAL_MODULE := $(BB_APPLETS_LIST)
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/utilities

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE) : $(LOCAL_PATH)/include/applet_tables.h
	mkdir -p $(dir $@)
	sed -E \
	-e "/const.*char.*applet_names/,/;/! d" \
	-e "{ s/[^\"]*[\"]// ; s/[;\"].*// }" \
	-e "/^$$/ d" \
	$< > $@


########################
